import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
// Exact copy except import UserService from core
import { Component, OnInit } from '@angular/core';
import { UserService } from '../core/user.service';

@Component({

    selector: 'app-login',
    templateUrl: './login.component.html'
})
export class LoginComponent {
    loginForm: FormGroup;
    username: FormControl;
    password: FormControl;

    constructor(builder: FormBuilder, private router: Router) {
        this.username = new FormControl('', [ Validators.required,
      Validators.minLength(5)]);
        this.password = new FormControl('', [Validators.required]);
        this.loginForm = builder.group({ username: this.username, password: this.password });
    }
    login() {
        console.log(this.loginForm.value);
        this.router.navigate(['/dashboard']);
    }

}


