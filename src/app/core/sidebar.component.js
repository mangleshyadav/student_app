"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Exact copy of app/title.component.ts except import UserService from shared
var core_1 = require('@angular/core');
var user_service_1 = require('../core/user.service');
var SidebarComponent = (function () {
    function SidebarComponent(userService) {
        this.subtitle = '';
        this.title = 'Angular Modules';
        this.user = '';
        this.user = userService.userName;
    }
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], SidebarComponent.prototype, "subtitle", void 0);
    SidebarComponent = __decorate([
        core_1.Component({
            selector: 'side-bar',
            templateUrl: './sidebar.component.html',
            styles: [" .list-group-item:first-child{\n    border-top-right-radius: 0rem;\n    border-top-left-radius: 0rem;\n  } \n .list-group-item:last-child{\n    border-bottom-right-radius: 0rem;\n    border-bottom-left-radius: 0rem;\n}\nul.side-bar > li {\n    border: 0px solid #ccc !important;\n}\n\n}\n\n "]
        }), 
        __metadata('design:paramtypes', [user_service_1.UserService])
    ], SidebarComponent);
    return SidebarComponent;
}());
exports.SidebarComponent = SidebarComponent;
//# sourceMappingURL=sidebar.component.js.map