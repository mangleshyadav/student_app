// Exact copy of app/title.component.ts except import UserService from shared
import { Component, Input } from '@angular/core';
import { UserService } from '../core/user.service';

@Component({

  selector: 'side-bar',
  templateUrl: './sidebar.component.html',
  styles: [` .list-group-item:first-child{
    border-top-right-radius: 0rem;
    border-top-left-radius: 0rem;
  } 
 .list-group-item:last-child{
    border-bottom-right-radius: 0rem;
    border-bottom-left-radius: 0rem;
}
ul.side-bar > li {
    border: 0px solid #ccc !important;
}

}

 `]
})
export class SidebarComponent {
  @Input() subtitle = '';
  title = 'Angular Modules';
  user = '';

  constructor(userService: UserService) {
    this.user = userService.userName;
  }
}
