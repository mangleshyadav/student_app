import { Component, OnInit } from '@angular/core';

import {
  Crisis,
  CrisisService
} from './crisis.service';

@Component({
  template: `
    <div class="card">
    <div class="card-header">
    <small highlight>Crisis List</small>

      <app-hello [name]="name"></app-hello>

    </div>
<div class="card-block">
  <small *ngIf="msg" class="msg">{{msg}}</small>

    <div *ngFor='let crisis of crisises | async'>
      <a routerLink="{{'../' + crisis.id}}">{{crisis.id}} - {{crisis.name}}</a>
</div>
</div>
    </div>
  `
})
export class CrisisListComponent implements OnInit {
  crisises: Promise<Crisis[]>;
  msg = 'Loading contacts ...';
  name = "mans"
  constructor(private crisisService: CrisisService) { }

  ngOnInit() {
    this.msg = '';
    this.crisises = this.crisisService.getCrises();
  }
}
