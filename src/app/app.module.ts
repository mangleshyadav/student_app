import { LoginModule } from './login/login.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

/* App Root */
import { AppComponent } from './app.component';

/* Feature Modules */

import { CoreModule } from './core/core.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { CrisisModule } from './crisis/crisis.module';
import { ContactModule } from './contact/contact.module';
import { StudentModule } from './student/student.module';
import { DemoModule } from './demo/demo.module';
/* Routing Module */
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    BrowserModule,
    LoginModule,
    DashboardModule,
    StudentModule,
    ContactModule,
    CrisisModule,

    /*
        CoreModule,
    */
    CoreModule.forRoot({ userName: 'Miss Marple' }),

    // RouterModule,
    AppRoutingModule
  ],
  declarations: [AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
