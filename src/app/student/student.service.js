"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var Student = (function () {
    function Student(id, name, std, div) {
        this.id = id;
        this.name = name;
        this.std = std;
        this.div = div;
    }
    return Student;
}());
exports.Student = Student;
var STUDENTS = [
    new Student(11, 'Mr. Nice', '5th', 'A'),
    new Student(12, 'Narco', '5th', 'B'),
    new Student(13, 'Bombasto', '5th', 'A'),
    new Student(14, 'Cameldeks', '5th', 'A'),
    new Student(15, 'Magneta', '5th', 'A'),
    new Student(16, 'Barmuda', '6th', 'A'),
    new Student(11, 'Mr. Nice', '6th', 'A'),
    new Student(12, 'Narco', '6th', 'B'),
    new Student(13, 'Narrottam', '6th', 'A'),
    new Student(14, 'Fox', '7th', 'A'),
    new Student(15, 'Magan', '7th', 'A'),
    new Student(16, 'Anaa', '7th', 'A'),
];
var FETCH_LATENCY = 500;
var StudentService = (function () {
    function StudentService() {
    }
    StudentService.prototype.getStudentes = function () {
        console.log(STUDENTS);
        return new Promise(function (resolve) {
            setTimeout(function () { resolve(STUDENTS); }, FETCH_LATENCY);
        });
    };
    StudentService.prototype.getStudent = function (id) {
        return this.getStudentes()
            .then(function (studentes) { return studentes.find(function (student) { return student.id === +id; }); });
    };
    StudentService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], StudentService);
    return StudentService;
}());
exports.StudentService = StudentService;
//# sourceMappingURL=student.service.js.map