import { Injectable } from '@angular/core';

export class Student {
  constructor(public id: number, public name: string, public std:string , public div:string  ) { }
}

const STUDENTS: Student[] = [
  new Student(11, 'Mr. Nice', '5th', 'A'),
  new Student(12, 'Narco' , '5th', 'B'),
  new Student(13, 'Bombasto' , '5th', 'A'),
  new Student(14, 'Cameldeks' , '5th', 'A'),
  new Student(15, 'Magneta' , '5th', 'A'),
  new Student(16, 'Barmuda', '6th', 'A'),
  new Student(11, 'Mr. Nice', '6th', 'A'),
  new Student(12, 'Narco' , '6th', 'B'),
  new Student(13, 'Narrottam' , '6th', 'A'),
  new Student(14, 'Fox' , '7th', 'A'),
  new Student(15, 'Magan' , '7th', 'A'),
  new Student(16, 'Anaa', '7th', 'A'),
];

const FETCH_LATENCY = 500;

@Injectable()
export class StudentService {

  getStudentes() {
    console.log(STUDENTS);
    return new Promise<Student[]>(resolve => {
      setTimeout(() => { resolve(STUDENTS); }, FETCH_LATENCY);
    });
  }

  getStudent(id: number | string) {
    return this.getStudentes()
      .then(studentes => studentes.find(student => student.id === +id));
  }

}
