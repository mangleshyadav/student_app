import { Component, OnInit } from '@angular/core';

import {
  Student,
  StudentService
} from './student.service';

@Component({
  templateUrl: './student-list.component.html',
  styles:[` ul.pagination {  display: -webkit-inline-box !important;} `]
})
export class StudentListComponent implements OnInit {
  studentes: Promise<Student[]>;
  msg = 'Loading contacts ...';
  data: any;
  public filterQuery = "";
  public rowsOnPage = 8;
  public sortBy = "name";
  public sortOrder = "asc";
  constructor(private studentService: StudentService) { }

  ngOnInit() {

    // this.data = this.studentService.getStudentes();

    this.studentService.getStudentes().then((data) => {

      this.data = data;
      console.log(this.data);
    });
  }
  displayMessage(msg: string) {
    this.msg = msg;
    setTimeout(() => this.msg = '', 1000);
  }

}
