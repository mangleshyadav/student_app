import { HelloComponent } from './hello.component';
import { NgModule } from '@angular/core';
import { DemodRoutingModule } from "./demo.routing";


@NgModule({
  imports: [DemodRoutingModule],
  declarations: [HelloComponent],
  exports: [HelloComponent],
  providers: []
})
export class DemoModule { }
