import { Component, trigger, state, style, transition, animate } from '@angular/core';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styles: [`
//   side-bar {
//     border-right: 1px solid rgba(0,0,0,.125);
// }
@media (max-width:480px){
  side-bar {
    position: absolute;
    z-index: 99;
    max-width: 300px;
    width: 300px !important;
    backface-visibility: inherit;
}`],
  animations: [
    trigger('slideInOut', [
      state('in', style({
        transform: 'translate3d(0, 0, 0)',
        width: '230px'

      })),
      state('out', style({
        transform: 'translate3d(-100%, 0, 0)',
        margin: '0px  0px 0px -130px'

      })),
      transition('in => out', animate('200ms ease-in-out')),
      transition('out => in', animate('200ms ease-in-out'))
    ]),
  ],

})
export class AppComponent {
  subtitle = '(Final)';

  menuState: string = 'in';
  toggleMenu() {
    // CSS classes: added/removed per current state of component properties
    this.menuState = this.menuState === 'out' ? 'in' : 'out';
  };
}
